﻿using System;
using System.Threading.Tasks;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Ios.Views;
using MvvmMarketplaceApp.Core.ViewModels;
using UIKit;

namespace MvvmMarketplaceApp.iOS.ViewControllers
{
    public partial class LoginView : MvxViewController<LoginViewModel>
    {
        public LoginView() : base(nameof(LoginView), null)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            var set = this.CreateBindingSet<LoginView, LoginViewModel>();
            set.Bind(InputUsername).To(vm => vm.Username);
            set.Bind(InputPassword).To(vm => vm.Password);
            set.Apply();
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);

            ButtonSignIn.TouchUpInside += ButtonSignIn_TouchUpInside;
        }

        public override void ViewWillDisappear(bool animated)
        {
            base.ViewWillDisappear(animated);

            ButtonSignIn.TouchUpInside -= ButtonSignIn_TouchUpInside;
        }

        async void ButtonSignIn_TouchUpInside(object sender, EventArgs e)
        {
            await SignIn();
        }

        async Task SignIn()
        {
            if (ViewModel.LoginWithUsernamePassword() == Core.LoginStatus.Successful)
            {
                await ViewModel.NavigateToLanding();
            }
            else if (ViewModel.LoginWithUsernamePassword() == Core.LoginStatus.Failed)
            {
                //Alert here 
            }
            else
            {
                //Different alert here
            }
        }
    }
}

