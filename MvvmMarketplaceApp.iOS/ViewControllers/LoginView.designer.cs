﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace MvvmMarketplaceApp.iOS.ViewControllers
{
    [Register ("LoginView")]
    partial class LoginView
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton ButtonSignIn { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField InputPassword { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField InputUsername { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (ButtonSignIn != null) {
                ButtonSignIn.Dispose ();
                ButtonSignIn = null;
            }

            if (InputPassword != null) {
                InputPassword.Dispose ();
                InputPassword = null;
            }

            if (InputUsername != null) {
                InputUsername.Dispose ();
                InputUsername = null;
            }
        }
    }
}