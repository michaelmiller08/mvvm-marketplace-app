﻿using System;
using System.Threading.Tasks;

namespace MvvmMarketplaceApp.Core.Services
{
    public interface ILoginService
    {
        LoginStatus LoginWithUsernamePassword(string username, string password);
    }
}
