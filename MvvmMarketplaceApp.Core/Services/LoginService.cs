﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Android.Widget;
using MvvmMarketplaceApp.Core.Repositories;

namespace MvvmMarketplaceApp.Core.Services
{
    public class LoginService : ILoginService
    {
        readonly IUserAccountRepository _userAccountRepository;

        public LoginService(IUserAccountRepository userAccountRepository)
        {
            _userAccountRepository = userAccountRepository;
        }

        public LoginStatus LoginWithUsernamePassword(string username, string password)
        {
            var status = LoginStatus.Failed;

            try
            {
                if (_userAccountRepository.UsernamePasswordCombinations().First(p => p.Username == username).Password == password)
                {
                    status = LoginStatus.Successful;
                }

            }
            catch (InvalidOperationException)
            {
                //Catch if the details are not found
            }

            return status;
        }
    }
}
