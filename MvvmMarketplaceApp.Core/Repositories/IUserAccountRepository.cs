﻿using System;
using System.Collections.Generic;
using MvvmMarketplaceApp.Core.Models;

namespace MvvmMarketplaceApp.Core.Repositories
{
    public interface IUserAccountRepository
    {
        IEnumerable<UsernamePasswordModel> UsernamePasswordCombinations();
    }
}
