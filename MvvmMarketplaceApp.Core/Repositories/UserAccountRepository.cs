﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MvvmMarketplaceApp.Core.Models;

namespace MvvmMarketplaceApp.Core.Repositories
{
    public class UserAccountRepository : IUserAccountRepository
    {
        readonly List<UsernamePasswordModel> _usernamesAndPasswords;

        public UserAccountRepository()
        {
            _usernamesAndPasswords = new List<UsernamePasswordModel>();

            InitializeRepository();
        }

        void InitializeRepository()
        {
            _usernamesAndPasswords.Add(CreateAccount("Test", "123"));
            _usernamesAndPasswords.Add(CreateAccount("1", "1"));
        }

        UsernamePasswordModel CreateAccount(string username, string password)
        {
            var usernameAndPassword = new UsernamePasswordModel
            {
                Username = username,
                Password = password
            };

            return usernameAndPassword;
        }

        public IEnumerable<UsernamePasswordModel> UsernamePasswordCombinations()
        {
            return _usernamesAndPasswords.AsEnumerable();
        }
    }
}
