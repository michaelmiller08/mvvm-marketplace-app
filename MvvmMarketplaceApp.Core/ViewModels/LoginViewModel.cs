﻿using System;
using System.Threading.Tasks;
using Android.App;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using MvvmMarketplaceApp.Core.Services;

namespace MvvmMarketplaceApp.Core.ViewModels
{
    public class LoginViewModel : MvxViewModel
    {
        string _username;
        string _password;

        readonly IMvxNavigationService _navigationService;
        readonly ILoginService _loginService;

        public LoginViewModel(ILoginService loginService, IMvxNavigationService navigationService)
        {
            _navigationService = navigationService;
            _loginService = loginService;
        }

        public override async Task Initialize()
        {
            await base.Initialize();
        }

        public string Username
        {
            get { return _username; }
            set
            {
                _username = value;
                RaisePropertyChanged(() => Username);
            }
        }

        public string Password
        {
            get { return _password; }
            set
            {
                _password = value;
                RaisePropertyChanged(() => Password);
            }
        }

        public LoginStatus LoginWithUsernamePassword()
        {
            return _loginService.LoginWithUsernamePassword(_username, _password);
        }

        public async Task NavigateToLanding()
        {
            await _navigationService.Navigate<LandingViewModel>();
        }
    }
}
