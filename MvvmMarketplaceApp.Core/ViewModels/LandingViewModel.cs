﻿using System;
using System.Threading.Tasks;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;

namespace MvvmMarketplaceApp.Core.ViewModels
{
    public class LandingViewModel : MvxViewModel
    {
        readonly IMvxNavigationService _navigationService;

        public LandingViewModel(IMvxNavigationService navigationService)
        {
            _navigationService = navigationService;
        }

        public override async Task Initialize()
        {
            await base.Initialize();
        }

        public async Task NavigateToHomeView()
        {
            await _navigationService.Navigate<HomeViewModel>();
        }

        public async Task NavigateToAccountView()
        {
            await _navigationService.Navigate<AccountViewModel>();
        }

        public async Task NavigateToDiscoverView()
        {
            await _navigationService.Navigate<DiscoverViewModel>();
        }

        public async Task NavigateToSettingsView()
        {
            await _navigationService.Navigate<SettingsViewModel>();
        }
    }
}
