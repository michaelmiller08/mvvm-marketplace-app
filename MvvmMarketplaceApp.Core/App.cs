﻿using System;
using System.Threading.Tasks;
using Android.App;
using MvvmCross;
using MvvmCross.ViewModels;
using MvvmMarketplaceApp.Core.Repositories;
using MvvmMarketplaceApp.Core.Services;
using MvvmMarketplaceApp.Core.ViewModels;

namespace MvvmMarketplaceApp.Core
{
    public class App : MvxApplication
    {
        public override void Initialize()
        {
            RegisterInterfaces();

            RegisterAppStart<LoginViewModel>();
        }

        void RegisterInterfaces()
        {
            //Services
            Mvx.IoCProvider.RegisterType<ILoginService, LoginService>();

            //Repositories
            Mvx.IoCProvider.RegisterType<IUserAccountRepository, UserAccountRepository>();
        }
    }
}
