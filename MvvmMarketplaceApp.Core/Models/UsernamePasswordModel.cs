﻿using System;
namespace MvvmMarketplaceApp.Core.Models
{
    public class UsernamePasswordModel
    {
        public String Username { get; set; }

        public String Password { get; set; }
    }
}
