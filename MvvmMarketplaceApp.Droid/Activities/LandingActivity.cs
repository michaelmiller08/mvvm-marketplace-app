﻿using Android.App;
using Android.OS;
using Android.Support.Design.Widget;
using MvvmCross.Droid.Support.V4;
using MvvmMarketplaceApp.Core.ViewModels;
using MvvmMarketplaceApp.Droid.Fragments;

namespace MvvmMarketplaceApp.Droid.Activities
{
    [Activity(Label = "LandingActivity")]
    public class LandingActivity : MvxFragmentActivity<LandingViewModel>
    {
        BottomNavigationView _bottomNavigation;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView(Resource.Layout.activity_landing);

            _bottomNavigation = FindViewById<BottomNavigationView>(Resource.Id.Bottom_Navigation);
            _bottomNavigation.NavigationItemSelected += _bottomNavigation_NavigationItemSelected;
        }

        protected override void OnStop()
        {
            base.OnStop();

            _bottomNavigation.NavigationItemSelected -= _bottomNavigation_NavigationItemSelected;
        }

        void _bottomNavigation_NavigationItemSelected(object sender, BottomNavigationView.NavigationItemSelectedEventArgs e)
        {
            DisplayFragment(LoadFragmentByResourceId(e.Item.ItemId));
        }

        MvxFragment LoadFragmentByResourceId(int id)
        {
            MvxFragment fragment = null;

            switch (id)
            {
                case Resource.Id.Menu_Home:
                    fragment = new HomeFragment();
                    break;
                case Resource.Id.Menu_Account:
                    fragment = new AccountFragment();
                    break;
                case Resource.Id.Menu_Discover:
                    fragment = new DiscoverFragment();
                    break;
                case Resource.Id.Menu_Settings:
                    fragment = new SettingsFragment();
                    break;
            }

            return fragment;
        }

        void DisplayFragment(MvxFragment fragment)
        {
            SupportFragmentManager.BeginTransaction()
                .Replace(Resource.Id.Landing_Fragment_container, fragment)
                .Commit();
        }
    }
}
