﻿using System.Threading.Tasks;
using Android.App;
using Android.OS;
using Android.Widget;
using MvvmCross.Platforms.Android.Views;
using MvvmMarketplaceApp.Core.ViewModels;

namespace MvvmMarketplaceApp.Droid.Activities
{
    [Activity( MainLauncher = true)]
    public class LoginActivity : MvxActivity<LoginViewModel>
    {
        Button _buttonSignIn;
        TextView _usernameField;
        TextView _passwordField;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView(Resource.Layout.activity_login);

            _buttonSignIn = FindViewById<Button>(Resource.Id.ButtonSignIn);
            _usernameField = FindViewById<TextView>(Resource.Id.InputUsername);
            _passwordField = FindViewById<TextView>(Resource.Id.InputPassword);

            SetHints();
        }

        protected override void OnStart()
        {
            base.OnStart();

            _buttonSignIn.Click += _buttonSignIn_Click;
        }

        protected override void OnStop()
        {
            base.OnStop();

            _buttonSignIn.Click -= _buttonSignIn_Click;
        }

        void SetHints()
        {
            _usernameField.Hint = Resources.GetString(Resource.String.username_hint);
            _passwordField.Hint = Resources.GetString(Resource.String.password_hint);
        }

        async void _buttonSignIn_Click(object sender, System.EventArgs e)
        {
            await SignIn();
        }

        async Task SignIn()
        {
            if (ViewModel.LoginWithUsernamePassword() == Core.LoginStatus.Successful)
            {
                await ViewModel.NavigateToLanding();
            }
            else if (ViewModel.LoginWithUsernamePassword() == Core.LoginStatus.Failed)
            {
                //Alert here 
            }
            else
            {
                //Different alert here
            }
        }

    }
}
