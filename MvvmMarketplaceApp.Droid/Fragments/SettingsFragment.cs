﻿using Android.OS;
using Android.Views;
using MvvmCross.Droid.Support.V4;
using MvvmMarketplaceApp.Core.ViewModels;

namespace MvvmMarketplaceApp.Droid.Fragments
{
    public class SettingsFragment : MvxFragment<SettingsViewModel>
    {
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            inflater.Inflate(Resource.Layout.fragment_settings, container);
            return base.OnCreateView(inflater, container, savedInstanceState);
        }
    }
}
