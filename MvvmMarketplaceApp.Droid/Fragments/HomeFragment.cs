﻿using Android.OS;
using Android.Views;
using MvvmCross.Droid.Support.V4;
using MvvmMarketplaceApp.Core.ViewModels;

namespace MvvmMarketplaceApp.Droid.Fragments
{
    public class HomeFragment : MvxFragment<HomeViewModel>
    {

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            inflater.Inflate(Resource.Layout.fragment_home, container);
            return base.OnCreateView(inflater, container, savedInstanceState);
        }
    }
}
